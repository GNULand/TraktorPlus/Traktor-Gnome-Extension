# Traktor Gnome Extension

A Gnome Shell Extension to switch the proxy mode between the pre-defined modes "Disable" and "Enable". you can restart tor service via this Extension.

![Screenshot](screenshot.png)

## Installation
```bash
$ wget https://gitlab.com/GNULand/TraktorPlus/Traktor-Gnome-Extension/repository/master/archive.zip -O Traktor-Gnome-Extension.zip
$ unzip Traktor-Gnome-Extension.zip -d $HOME/Traktor-Gnome-Extension && cd $HOME/Traktor-Gnome-Extension/*
$ make build
$ sudo make install
```
## Uninstallation
```bash
$ sudo rm -rf /usr/share/gnome-shell/extensions/Traktor-Extension
$ sudo rm -rf /usr/share/applications/traktor-extension.desktop
```
## Changes
[See Changes](https://gitlab.com/GNULand/TraktorPlus/Traktor-Gnome-Extension/blob/master/CHANGELOG)
